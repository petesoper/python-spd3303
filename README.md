# python-spd3303

##Preface
If you already own an SPD3303S, jump across the next section and share my sadness while hopefully making some use of these tools.

##The 12 reasons you should not dream of buying a Siglent SPD3303S

1. Sometimes when the voltage control knob is turned counter clockwise the voltage output goes UP INSTEAD OF DOWN. It's possible the current control behaves the same way, but I haven't observed it like the dozen times I've observed voltage output misbehavior.
2. Sometimes when the voltage or current control knob is turned the "meaning" of a single knob click changes from the expected small increment to a much MUCH LARGER increment. This can cause your stepping of voltage into a 3.3 volt part for example, to suddenly be applying a much higher voltage to the part. Watch the magic smoke escape.
3. The screen blanks after a short period of use and this cannot be disabled as far as I can tell.
4. The banana jacks do not have standard dimensions between hole centers, making your existing collection of double plugs worthless.
5. Traditional, common single banana plugs don't seat properly in this power supply's jacks. Compare this, for example, to the jacks in every DMM built in the past fifty years.
6. The USB communication is unreliable. There appear to be undocumented requirements for time delays after some forms of communciation with the power supply. These superstitiously applied delays appear to prevent the supply from ignoring queries and commands at least some of the time. Remotely controlling this power supply in a sensitive, let alone hazardous situation would not be prudent.
7. Sometimes when pressing the control knob's momentary contact switch (i.e. pressing the knob in and then releasing), instead of stepping between voltage, current, and timer settings, the current value (e.g. voltage) is incremented. As with so many other aspects of the UI, the effect is to prevent the user from ever relaxing and trusting that a set of actions might create the same effect as the previous time those actions were used. So, pressing the knob twice and then turning it to engage the timer results in seeing that instead the voltage or current limits are being incremented!!! The firmware programmer and/or hardware designer responsible for this FUBAR needs to find some other line of work if he/she has not already. Gardening, perhaps.
8. The SCPI documentation stinks particularly bad.
9. The documentation stinks.
10. The vendor I bought this power supply from (Tequipment) has dropped Siglent and I take this as a strong hint.
11. Because of (6), the performance via remote control is converted from slow to very slow.
12. The rev 1 user manual states:
   "Chapter 4 Troubleshooting
      Question 1: what to do if there occurs a short circuit on output terminal?
      Answer1: There are over current protection and short circuit protection inside the power, so the current will be controlled in safety range."
     BZZZZZT! WRONG! (Don't) try connecting to a battery to charge it in dim light, getting the connection backwards. BLAM. Dead channel. I now have half an SPD3303S. Now, then, where is the service manual? I have a service manual for my Siglent SPG1050 function generator (that I am happy with, so far, by the way). But there is no sign of a service manual for the power supply. Waiting now for a reply to a note sent to Siglent about this.

## Introduction
This repo has Python code for remote USB control of a Siglent SPD3303S power supply. The lead acid charger program has been testing pretty well but the beefing up of its error handling when the power supply barfs down its USB port has not een applied to the lithium charger yet. The testing of the lithium charger is by no means complete, either, so be careful with that. Because of "flaw #6" above I may never declare the charger programs fully safe to use. I'll need to see many thousands of error free USB queries/commands before that step. But again, this code might be of use in any case.

## Contents
1. An exercise/test program
2. A program that controls the power supply to charge lead acid batteries
3. Ditto for lithium batteries (Note, not sync'd with lead acid: need API now!)

## Future Plans
1. Finish the apps above and finish testing to "promote" versions to a "released" state.
2. Make an API and retrofit the apps to use the API
3. NiMH charging support, maybe
4. Unification of the charger programs into a single one
5. Abstraction of the PS API calls from the charger program to make it practical to use other power supplies
6. Integrate use of other instrument measurements, starting with temperature measurement to make the charger program safer to use


