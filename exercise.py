#!/usr/bin/python 
#
# spd3303 exercise
#
# Exercise USB control of the Siglent SPD3303S power supply. This might work
# equally well for the other SPD3303D power supplies. It might also work for
# the SPD3303C, D, X, and X-E. Actually, this code was written with the only
# 3303 manual I could find that covers the SCPI commands and it isn't really
# clear which model the manual is aimed at! The Siglent documentation is not
# consistent with their power supplies (in a word, it is feeble).
#
# The program is intended to step through a series of default or commmand-line 
# selected voltage and current output limits and measure actual current and 
# voltage at it's output channels. By default trimmed resistors are assumed 
# connected across the outputs in a simple configuration (i.e. both channels 
# individually controlled, one pair to be used in parallel, or both to be used 
# in series).
# 
# By default a 250 ohm power resistor that can handle four watts should be 
# connected across each channel's output (i.e. one and two).
#
# The test configuration name 'test_spd' is used to exercise the save/restore
# of PS state.
#
# A verbosity level option allows control over how much test detail is shown.
#
# Finally, once the feature set and and testing are reasonably complete an
# API will be developed as a separate code module and this program will be
# retrofitted to use the API calls. It will then constitute a collection of
# examples of the API usage as well as a validation program for the API.
#
# This API will naturally support a more flexible test program that can
# coordinate operation of the power supply with operation of a complementary
# instrument such as an electronic load.
#
#
# Implementation Plan ('*' is latest completed)
#
#  Rev       Target
#
#  0.01      Initial commmit
#  0.02      Trivial go/go not default test that cycles a 1k resistor on each
#             channel and confirms expected performance for a small number of
#             output combinations. 
#  0.03      -s for showing ps settings & measured output
# *0.04      Updated plan and supporting doc
#  0.10      Support parallel connection. Requires load that can tolerate
#             six amps at 30 volts for a proper go/no go test.
#  0.20      Support serial connection. Requires load that can tolerate 60
#             volts at three amps for a proper go/no go test.
#  0.30      Adjustable tolerances to support other models.
#  0.40      Exercise all defined SCPI commands
#  0.50      Use API calls.
#  1.00      Testing complete
#
# (C) Peter Soper (pete@soper.us) 2016  MIT License
# September, 2016
#
#usage: spd3303-exercise.py [-h] [-V] [-v VERBOSITY_LEVEL] [-s] [-t TIME_LIMIT]
#                           [-u USB_PATH]
#
#Siglent SPD3303S Exerciser
#
#optional arguments:
#  -h, --help            show this help message and exit
#  -V, --Version         Display version
#  -v VERBOSITY_LEVEL, --verbosity_level VERBOSITY_LEVEL
#                        Debug level, default 0
#  -s, --show_status     Show status and quit
#  -t TIME_LIMIT, --time_limit TIME_LIMIT
#                        Running time limit in seconds, default DEFAULT_RUNNING_TIME_LIMIT
#  -u USB_PATH, --usb_path USB_PATH
#                        USB (instrument) device pathname, default DEFAULT_USB_PATH

import argparse, os, sys, time, datetime

VERSION_STRING = '0.04'

SINGLE_CHANNEL_CURRENT_LIMIT = 3.0
SINGLE_CHANNEL_VOLTAGE_LIMIT = 30.0
DEFAULT_VERBOSITY_LEVEL = 0
CHANNEL_COUNT = 2
PS_WRITE_DELAY = 0.05
PARALLEL_CURRENT_LIMIT = SINGLE_CHANNEL_CURRENT_LIMIT * CHANNEL_COUNT
SERIES_VOLTAGE_LIMIT = SINGLE_CHANNEL_VOLTAGE_LIMIT * CHANNEL_COUNT

CHANNEL_PREFIX = 'CH'
CHANNEL_ONE = CHANNEL_PREFIX + '1'
CHANNEL_TWO = CHANNEL_PREFIX + '2'

CHANNELS = [CHANNEL_ONE, CHANNEL_TWO]

FLOAT_EQUALITY_PERCENT = 0.5    # "close enough" % between two float values

DEFAULT_RUNNING_TIME_LIMIT = 20 # Default running time limit in seconds
running_time_limit = DEFAULT_RUNNING_TIME_LIMIT        # Max running time

DEFAULT_USB_PATH = '/dev/usbtmc1'		       # USBTMC interface for PS
ps = None			# Python Serial object for power supply
start_time = int(time.time())	# Starting Unix Epoch seconds since Jan 1, 1970
running_time = 0		# Elapsed time
verbosity_level = DEFAULT_VERBOSITY_LEVEL

class SPDException(Exception):
    "Exception base class"

class SPDReadException(SPDException):
    "Failed to complete an SCPI response read from the power supply"
 
class SPDWriteException(SPDException):
    "Failed to complete an SCPI command write to the power supply"
 
class SPDPerformanceException(SPDException):
    "Measured performance outside specification"
 
class SPDIllegalStatusException(SPDException):
    "Power Supply status register illegal value"
 
def shutdown(msg = '', exception = None):
    "Print optional message, turn off power supply channel, close paths, raise optional exception or just quit"
    if msg != '':
        flush_print(msg)
    supply_off(CHANNEL_ONE, True)
    supply_off(CHANNEL_TWO, True)
    os.close(ps)
    if exception != None:
	raise exception
    else:
        quit()

def flush_print(s, console_only = False):
    "Print a string to standard output and flush it"
    print s
    sys.stdout.flush()

def equal_values(v1, v2, tolerance_percent = FLOAT_EQUALITY_PERCENT):
    "returns true if v1 approximately equal to v2"
    temp = (abs(v1 - v2) / max(v1, v2) * 100.0) < tolerance_percent
    if not temp and verbosity_level > 2:
	flush_print('equal_values ' + str(v1) + ' != ' + str(v2))
    return temp

def elapsed_time():
   "Return float seconds since execution start (object creation with API)"
   return time.time() - start_time

def write_inst(s, bail_on_error = False):
    "Write SCPI command to PS. Failures handled at an outer level.
    if verbosity_level > 1:
        flush_print('write_inst: ' + s)
    try:
        os.write(ps, s)
    except OSError, argument:
        if verbosity_level > 1:
            flush_print('tried to write ' + s + ' but got a write OSError: ' + 
		  argument)
    # This appears to be critically important, but the actual delay needed
    # is still a mystery.
    time.sleep(PS_WRITE_DELAY)

def read_inst(cmd):
    "Read string from device, retrying write of cmd if read failure"
    if verbosity_level > 1:
        flush_print('read_inst: ' + cmd, True)
    try:
        s = os.read(ps, 300)
    except OSError:
        if verbosity_level > 1:
            flush_print('read error: repeating query: ' + cmd)
        # Retry write
        write_inst(cmd)
	# Recursively retry the read
        s = read_inst(cmd)
    if verbosity_level > 1:
        flush_print('read_inst returning: ' + s, True)
    return s

def ask(cmd):
    "Write SCPI query and return response value"
    write_inst(cmd)
    time.sleep(PS_WRITE_DELAY)
    return read_inst(cmd)
 
def get_current(channel):
    "Return PS output current"
    if verbosity_level > 2:
	flush_print('get_current')
    return float(ask('MEAS:CURR? ' + channel))

def get_voltage(channel):
    "Return PS output voltage"
    if verbosity_level > 2:
	flush_print('get_voltage')
    return float(ask('MEAS:VOLT? ' + channel))

def get_current_limit(channel):
    "Return PS current limit"
    if verbosity_level > 2:
        flush_print('get_current_limit channel type: ' + str(type(channel)))
        flush_print('get_current_limit command: ' + channel + ':CURR?')
    return float(ask(channel + ':CURR?'))

def get_voltage_limit(channel):
    "Return PS voltage limit"
    return float(ask(channel + ':VOLT?'))

def set_current_limit(current, channel):
    "Change current limit"
    if verbosity_level > 1:
	flush_print('set_current_limit: ' + str(current) + ' channel: ' + str(channel))
    while True:
        write_inst(channel + ':CURR '+ str(current))
        if equal_values(get_current_limit(channel), current):
            return
        else:
	    raise SPDPerformanceException(str(get_current_limit(channel)) + ' != ' + str(current))

def set_voltage_limit(voltage, channel):
    "Change voltage limit"
    if verbosity_level > 1:
	flush_print('set_voltage_limit: ' + str(voltage) + ' channel: ' + str(channel))
    while True:
	write_inst(channel + ':VOLT ' + str(voltage))
	measured_voltage_limit = get_voltage_limit(channel)
	if equal_values(measured_voltage_limit, voltage):
	    return
	else:
	    raise SPDPerformanceException(str(measured_voltage_limit) + ' != ' + str(voltage))
	if verbosity_level > 0:
	    flush_print('set_voltage_limit retry')

def supply_on(channel):
    "Turn on the supply channel"
    if verbosity_level > 2:
	flush_print('supply on')
    write_inst('OUTP ' + channel + ', ON')

def supply_off(channel, bail_on_error = False):
    "Turn off the supply channel"
    if verbosity_level > 2:
	flush_print('supply on')
    write_inst('OUTP ' + channel + ', OFF', bail_on_error)

def prepare_state(channel, current_limit, voltage_limit):
    set_current_limit(current_limit, channel)
    set_voltage_limit(voltage_limit, channel)
    supply_on(channel)
    time.sleep(0.1)

def trial(channel, current_limit, voltage_limit, expected_current, 
	  expected_voltage, use_saved_state = False)
    "Run one test trial"

    if use_saved_state:
	write_inst('*RCL test_spd')
    else:
	prepare_state(channel, current_limit, volage_limit)
        write_inst('*SAV test_spd')

    time.sleep(0.1)
    current = get_current(channel)
    time.sleep(0.1)
    voltage = get_voltage(channel)
    time.sleep(0.1)
    supply_off(channel)

    if verbosity_level > 0:
	flush_print('trial channel: ' + channel +
	    ' current_limit: ' + str(current_limit) +
	    ' voltage_limit: ' + str(current_limit) +
	    ' current: ' + str(current) +
	    ' voltage: ' + str(voltage))

    if not equal_values(current, expected_current) or not equal_values(voltage, expected_voltage):
	terminate('Test trial failed', 
	       SPDPerformanceException(
		    str(current) + 'A vs ' + str(expected_current) + 'A ' +
		    str(voltage) + 'V vs ' + str(expected_voltage) + 'V'))

def check_id(expected_model):
    id = ask('*IDN?')
    if string.find(id, expected_model) == -1:
	terminate('Unexpected model', 
	          SPDPerformanceException('Expected: ' + expected_model +
		  ' saw: ' + id))

def show_status():
    status = int(ask('SYST:STAT?'), 16)
    line = ''
    measure_ch1 = measure_ch2 = False
    mode = (status & 0xc) >> 2
    if mode == 1:
	line += 'Independent '
    elif mode == 2:
	line += 'Parallel '
    elif mode == 3:
	line += 'Serial '
    else:
	raise SPDIllegalStatusException('{0:x}'.format(status))
    if mode == 1:
	line += 'CH1 '
    if (status & 0b10001) == 0b10001:
	line += 'CC '
	measure_ch1 = True
    elif (status & 0b10001) == 0b10000:
	line += 'CV '
	measure_ch1 = True
    if mode == 1:
	if (status & 0b100010) == 0b100010:
	    line += 'CH2 CC '
	    measure_ch2 = True
	elif (status & 0b100010) == 0b100010:
	    line += 'CH2 CV '
	    measure_ch2 = True
    if status & 0b1000000:
	line += 'TIMER1 '
    if status & 0b10000000:
	line += 'TIMER2 '
    if mode == 1:
	line += 'CH1 '
    if status & 0b100000000:
	line += 'Waveform '
    else:
	line += 'Digital '
    if mode == 1:
	if status & 0b1000000000:
	    line += 'CH2 Waveform '
	else:
	    line += 'CH2 Digital '
    if measure_ch1:
	line += 'CH1: ' + str(get_voltage(CHANNEL_ONE)) + 'V '
	line += str(get_current(CHANNEL_ONE)) + 'A '
    if measure_ch2:
	line += 'CH2: ' + str(get_voltage(CHANNEL_TWO)) + 'V '
	line += str(get_current(CHANNEL_TWO)) + 'A '
    flush_print(line)

if __name__ == '__main__':
    "Handle command line arguments(options) and run exercise cycles"

    start_date_time = datetime.datetime.now()
    start_date_time = start_date_time.isoformat(sep=' ')

    parser = argparse.ArgumentParser(
	description='Siglent SPD3303S Exerciser')

    parser.add_argument(
	'-V', '--Version', help='Display version', action='store_true')

    parser.add_argument(
	'-v', '--verbosity_level', default=str(DEFAULT_VERBOSITY_LEVEL), help='Debug level, default 0')

    parser.add_argument(
	'-s', '--show_status', help='Show status and quit', action='store_true')

    parser.add_argument(
	'-t', '--time_limit', default=str(DEFAULT_RUNNING_TIME_LIMIT), 
	help='Running time limit in seconds, default ' + 
	str(DEFAULT_RUNNING_TIME_LIMIT))

    parser.add_argument(
	'-u', '--usb_path', default=DEFAULT_USB_PATH, help=
	'USB (instrument) device pathname, default ' + str(DEFAULT_USB_PATH))

    args = parser.parse_args()
    if args.Version:
	flush_print(VERSION_STRING)
	quit()

    time_limit = float(args.time_limit)
    verbosity_level = int(args.verbosity_level)

    ps = os.open(args.usb_path, os.O_RDWR)

    if args.show_status:
	show_status()
	quit()

    trial_count = 0
    try:
	write_inst('
        while elapsed_time() < time_limit:
            trial(CHANNEL_TWO, 0.005, 1.0, 0.0041, 1.0)
            trial(CHANNEL_TWO, 0.050, 10.0, 0.041, 10.0)
            trial(CHANNEL_TWO, 0.100, 20.0, 0.082, 20.0)
            trial(CHANNEL_TWO, 0.150, 30.0, 0.123, 30.0)
	    prepare_state(CHANNEL_TWO, 0.001, 1.0)
	    supply_off(CHANNEL_TWO)
            trial(CHANNEL_TWO, 0.150, 30.0, 0.123, 30.0, True)
	    trial_count += 1
	    if verbosity_level > 1:
	        flush_print str(elapsed_time())
    except KeyboardInterrupt:
        terminate('Interrupt', KeyboardInterrupt(start_date_time))

    terminate('Normal Completion of ' + str(trial_count) + ' trials', None)

