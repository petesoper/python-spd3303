#!/usr/bin/python 
#
# Siglent power supply API
#
# Interfaces to the SPCI
#
# (C) Peter Soper (pete@soper.us) MIT License
# November, 2016
#

import argparse, os, sys, time, datetime

VERSION_STRING = '0.01'

FLOAT_EQUALITY_PERCENT = 1      # "close enough" % between two float values
RATED_CAPACITY = 7.0		# Battery amp hours
rated_capacity = RATED_CAPACITY # Can be overriden

CURRENT_LIMIT = RATED_CAPACITY * 0.2 # Current limit during bulk state
current_limit = CURRENT_LIMIT	# Can be overriden 

ITAPER = RATED_CAPACITY * 0.02  # Min current to reach before bulk=>float
itaper = ITAPER			# Can be overriden 
FLOAT_CURRENT = ITAPER * 2.0
SAMPLE_DELAY = 10.0

PS_CHARGER_CHANNEL = 'CH1'	# Which PS channel used as charger
debug_level = 0			# Debug/verbosity level
LOOP_COUNT = 2		        # Debug mode creates artificially short states
TRICKLE_THRESHOLD = 10.5        # Battery voltage to get out of trickle state
TRICKLE_CURRENT = 0.025         # Charge current in trickle state
				# was 14.45 until 9/6
BULK_VOLTAGE = 14.4	        # Bulk charge voltage
FLOAT_VOLTAGE = 13.6	        # Float charge voltage
FLOAT_ONLY_CURRENT_LIMIT = CURRENT_LIMIT# Max current when -f option used
FLOAT_ONLY_CURRENT_LIMIT_DURATION = 10800 # Max time max current allowed w -f
DEFAULT_CHARGING_TIME_LIMIT = 3600*7     # Charging time limit
DEFAULT_BULK_TIME_LIMIT = 3600*5          # Bulk state time limit
MAX_CURRENT_RISE = 100.0	# Percent current rise allowed during bulk state
PS_WRITE_DELAY = 0.25           # Seconds to pause after write to PS

charging_time_limit = DEFAULT_CHARGING_TIME_LIMIT      # Max time for a charge
bulk_time_limit = DEFAULT_BULK_TIME_LIMIT              # Max time spent in bulk
DEFAULT_USB_PATH = '/dev/usbtmc1'		       # USBTMC interface for PS
DEFAULT_LOG_PATH = 'spd3303s-charger.log' # Log output path
out = None			# Logging output file
pwr = None			# Python Serial object for power supply
start_time = int(time.time())	# Unix Epoch seconds since Jan 1, 1970
coulombs = 0.0			# Cumulative amp-seconds
wattseconds = 0.0		# Cumulative

class SPDException(Exception):
    "Exception base class"

class SPDWriteException(SPDException):
    "Failed to complete an SCPI command write to the power supply"
 
def float_equals(v1, v2):
    diff = abs(v1 - v2)
    if v1 > v2:
        temp = v1
    else:
        temp = v2
    return ((diff / temp) * 100.0) < FLOAT_EQUALITY_PERCENT

def elapsed_time():
   "Return seconds since execution start"
   return int(time.time()) - start_time

def myprint(s, console_only = False):
    "Print a string and flush output"
    print s
    sys.stdout.flush()
    if not console_only:
        try:
            out.write('{0:s}\n'.format(s))
	except KeyboardInterrupt:
            finish('Keyboard Interrupt')
        except OSError:
            finish('tried to write to log but got OSError') 
        except Exception as e:
            finish('tried to write to log but got unknown error')


def write_inst(s, no_exception_handling = False):
    "Write SCPI command to device. A write error is currently fatal."
    global pwr
    if debug_level > 0:
	myprint('write_inst: ' + s, True)
    if no_exception_handling:
        os.write(pwr, s)
	return
    try:
        os.write(pwr, s)
    except KeyboardInterrupt:
        finish('Keyboard Interrupt')
    except OSError:
        finish('tried to write ' + s + ' but got a write error')
    except Exception as e:
        finish('tried to write ' + s + ' but got unknown error')

def read_inst(cmd):
    "Write SCPI command to device, retrying as needed to compensate for Siglent"
    global pwr
    if debug_level > 0:
        myprint('read_inst: ' + cmd, True)
    try:
        s = os.read(pwr, 300)
    except KeyboardInterrupt:
        finish('Keyboard Interrupt')
    except OSError:
        myprint('read error: repeating query: ' + cmd)
        write_inst(cmd)
        time.sleep(PS_WRITE_DELAY)
        s = read_inst(cmd)
    except Exception as e:
        finish('tried to write ' + s + ' but got unknown error')
    if debug_level > 0:
        myprint('read_inst returning: ' + s, True)
    return s

def ask(cmd):
    "Write SCPI query and return response value"
    write_inst(cmd)
    time.sleep(PS_WRITE_DELAY)
    return read_inst(cmd)
 
def get_current():
    "Return PS output current"
    return float(ask('MEAS:CURR? ' + PS_CHARGER_CHANNEL))

def get_voltage():
    "Return PS output voltage"
    return float(ask('MEAS:VOLT? ' + PS_CHARGER_CHANNEL))

def get_current_limit():
    "Return PS current limit"
    return float(ask(PS_CHARGER_CHANNEL + ':CURR?'))

def get_voltage_limit():
    "Return PS voltage limit"
    return float(ask(PS_CHARGER_CHANNEL + ':VOLT?'))

def set_current_limit(current):
    "Change current limit"
    while True:
	try:
            write_inst(PS_CHARGER_CHANNEL + ':CURR '+ str(current))
            time.sleep(PS_WRITE_DELAY)
            if float_equals(get_current_limit(), current):
  	        return
        except KeyboardInterrupt:
            finish('Keyboard Interrupt')
	except:
	    supply_off()
            raise SPDWriteException

def set_voltage_limit(voltage):
    "Change voltage limit"
    while True:
	try:
            write_inst(PS_CHARGER_CHANNEL + ':VOLT ' + str(voltage))
            time.sleep(PS_WRITE_DELAY)
            if float_equals(get_voltage_limit(), voltage):
  	        return
        except KeyboardInterrupt:
            finish('Keyboard Interrupt')
	except:
	    supply_off()
            raise SPDWriteException

def supply_on():
    "Turn on the supply channel"
    write_inst('OUTP ' + PS_CHARGER_CHANNEL + ', ON')
    time.sleep(1)
    myprint('Supply on', True)

def supply_off():
    "Turn off the supply channel"
    global coulombs, wattseconds
    write_inst('OUTP ' + PS_CHARGER_CHANNEL + ', OFF', True)
    time.sleep(1)
    myprint('Supply off', True)
    myprint('Total coulombs: ' + str(coulombs) + 
	    ' Total wattseconds: ' + str(wattseconds))
    coulombs = wattseconds = 0.0

def finish(msg):
    "Print optional message, turn off power supply channel, close paths, quit"
    if msg != '':
        print(msg)
    supply_off()
    os.close(pwr)
    out.close()
    quit()

def log(interval_start, return_current = True):
    global coulombs, wattseconds
    current = get_current()
    voltage = get_voltage()
    interval = time.time() - interval_start
    interval_coulombs = current * interval
    coulombs += interval_coulombs
    interval_wattseconds = current * voltage * interval
    wattseconds += interval_wattseconds
    now = datetime.datetime.now().isoformat(sep=' ')
    myprint('{0:s} {1:7.3f} {2:7.3f} {3:6.2f} {4:6.2f} {5:s}'.format(str(elapsed_time()),
	    voltage, current, coulombs / 3600.0, wattseconds / 3600.0, now))
    if return_current:
	return current
    else:
	return voltage

# State 1: trickle. While battery voltage lower than 10.5 volts apply
# 25 milliamperes

def trickle_state():
    "Charger Trickle State"
    if debug_level > 0:
	myprint("Trickle", True)
    set_current_limit(TRICKLE_CURRENT)
    set_voltage_limit(BULK_VOLTAGE)
    supply_on()
    current = get_current()
    voltage = get_voltage()
    if debug_level > 1:
        myprint('current: ' + str(current) + ' voltage: ' + str(voltage), True)
    while voltage < TRICKLE_THRESHOLD:
	interval_start = time.time()
        if elapsed_time() >= charging_time_limit:
	    finish('charging time limit reached')
	time.sleep(SAMPLE_DELAY)
        voltage = log(interval_start, False)

# State 2: Bulk charge until current drops to Itaper.
# Watch out for current switching from falling back to rising. Detect this and
# stop the charger if detected.

def bulk_state():
    "Bulk Charge State."
    if debug_level > 0:
	myprint("Bulk", True)
    relative_start_bulk = int(time.time()-start_time)
    set_current_limit(current_limit)
    set_voltage_limit(BULK_VOLTAGE)
    time.sleep(1.0)
    current = get_current()
    minimum_current = current
    voltage = get_voltage()
    myprint('current: ' + str(current) + ' voltage: ' + str(voltage), True)
    if debug_level > 1:
        myprint('current: ' + str(current) + ' voltage: ' + str(voltage), True)
    if debug_level > 0:
        myprint('Starting voltage ' + str(voltage), True)
        myprint('Starting current ' + str(current))
    while current > itaper:
	interval_start = time.time()
        current = get_current()
	relative_time = elapsed_time()
        if relative_time >= charging_time_limit:
	    finish('charging time limit reached')
        if relative_time >= bulk_time_limit:
	    finish('bulk charging time limit reached')
	# Don't tolerate increase in current
        if current < minimum_current:
	    minimum_current = current
        elif current > minimum_current:
	    if (((current - minimum_current) / minimum_current) * 
	        100.0) > MAX_CURRENT_RISE:
	        finish('Bulk charge aborted due to current rise')
	time.sleep(SAMPLE_DELAY)
	current = log(interval_start, True)


# State 3: Float charge

def float_state(is_just_float):
    "Float Charge State."
    if debug_level > 0:
	myprint("Float", True)
    set_voltage_limit(FLOAT_VOLTAGE)
    set_current_limit(FLOAT_CURRENT)
    if is_just_float:
        set_current_limit(FLOAT_ONLY_CURRENT_LIMIT)
        supply_on()
    while True:
	interval_start = time.time()
	relative_time = elapsed_time()
        if relative_time >= charging_time_limit:
	    finish('charging time limit reached')
	time.sleep(SAMPLE_DELAY)
	# If in float state following bulk, set limit at max current
        # Don't remember what this is. Looks useless
        #if not is_just_float:
	#    if (current >= FLOAT_ONLY_CURRENT_LIMIT):
	#        limit_duration += 1
	#        if limit_duration >= FLOAT_ONLY_CURRENT_LIMIT_DURATION:
	#            finish(
	#		'Float current limit for ' + 
	#		str(FLOAT_ONLY_CURRENT_LIMIT_DURATION) + ' seconds')
	log(interval_start)

if __name__ == '__main__':
    "Handle command line arguments(options) and run charging states in order"

    parser = argparse.ArgumentParser(
	description='Siglent SPD3303S-based Lead Acid Charger ' + 'rev ' +
	VERSION_STRING)

    parser.add_argument(
	'-b', '--bulk_time_limit', default=str(DEFAULT_BULK_TIME_LIMIT), 
	help='Bulk Charge time limit in seconds, default ' + 
	str(DEFAULT_BULK_TIME_LIMIT))

    parser.add_argument(
	'-c', '--current_limit', default=str(CURRENT_LIMIT), 
	help='Bulk current limit in amps, default ' + 
	str(CURRENT_LIMIT))

    parser.add_argument(
	'-d', '--debug_level', default='0', help='Debug level, default 0')

    parser.add_argument(
	'-f', '--float', help='Skip straight to float state', 
	action='store_true')

    parser.add_argument(
	'-i', '--itaper', default=str(ITAPER), 
	help='Minimum taper current for bulk=>float transistion, default ' + 
	str(ITAPER))

    parser.add_argument(
	'-n', '--no_float', help='No float state', action='store_true')

    parser.add_argument(
	'-o', '--output', default=DEFAULT_LOG_PATH, help=
	'Log output pathname, default ' + str(DEFAULT_LOG_PATH))

    parser.add_argument(
	'-r', '--rated_capacity', default=RATED_CAPACITY, 
        help='Battery rated capacity, default ' + str(RATED_CAPACITY))

    parser.add_argument(
	'-t', '--time_limit', default=str(DEFAULT_CHARGING_TIME_LIMIT), 
	help='Charging time limit in seconds, default ' + 
	str(DEFAULT_CHARGING_TIME_LIMIT))

    parser.add_argument(
	'-u', '--usb_path', default=DEFAULT_USB_PATH, help=
	'USB (instrument) device pathname, default ' + str(DEFAULT_USB_PATH))

    args = parser.parse_args()

    debug_level = int(args.debug_level)
    bulk_time_limit = int(args.bulk_time_limit)
    charging_time_limit = int(args.time_limit)

    if not float_equals(args.rated_capacity, RATED_CAPACITY):
	rated_capacity = float(args.rated_capacity)
	current_limit = rated_capacity * 0.1
	itaper = rated_capacity * 0.02
    if not float_equals(float(args.current_limit), CURRENT_LIMIT):
	current_limit = float(args.current_limit)
    if not float_equals(float(args.itaper), ITAPER):
	itaper = float(args.itaper)

    myprint('open: ' + args.usb_path + ' ' + args.output, True)
    pwr = os.open(args.usb_path, os.O_RDWR)
    out = open(args.output, 'a', 0)

    myprint('SPD-3303S Programmed Lead Acid Charger', True)
    myprint('USB Path: ' + args.usb_path, True)
    myprint('LOG Path: ' + args.output, True)
    myprint('Rated battery capacity: ' + str(rated_capacity) + ' aH', True)
    myprint('Bulk current limit: ' + str(current_limit) + ' A', True)
    myprint('Bulk current taper cuttoff: ' + str(itaper) + ' A', True)
    myprint('Trickle current: ' + str(TRICKLE_CURRENT) + ' A', True)
    myprint('Trickle threshold: ' + str(TRICKLE_THRESHOLD) + ' V', True)
    myprint('Bulk charge voltage: ' + str(BULK_VOLTAGE) + ' V', True)
    myprint('Float voltage: ' + str(FLOAT_VOLTAGE) + ' V', True)

    if not args.float:
        trickle_state()
        bulk_state()
	if not args.no_float:
            float_state(False)
    else:
        float_state(True)

    finish('')

