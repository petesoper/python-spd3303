#!/usr/bin/python 
#
# This is untested, unreleased software. Using this in its current state could 
# be hazardous to your house or office (fire!)
#
# Siglent power supply-based battery charger for lithium battteries. Default
# charge voltage is 4.2 volts. Default C is 6000mAh. Default peak charge 
# current is .2C. Default cutoff current .01C. 
#
# Implementation Plan
#  (* indicates current version)
#
# Rev             Goal
# 0.05            Initial release
#*0.15            Coulomb counting
# 0.20            Retrofit planned PS interface API
# 0.25            Support for temperature monitoring
#
# (C) Peter Soper (pete@soper.us) MIT License
# September, 2016
#
#usage: lithium-charger.py [-h] [-c CURRENT_LIMIT] [-d DEBUG_LEVEL]
#                          [-m MIN_CURRENT] [-n] [-o OUTPUT]
#                          [-p PER_CHARGE_LIMIT] [-r RATED_CAPACITY]
#                          [-t TIME_LIMIT] [-u USB_PATH]
#
#Siglent SPD3303S-based Lithium Battery Charger
#
#optional arguments:
#  -h, --help            show this help message and exit
#  -c CURRENT_LIMIT, --current_limit CURRENT_LIMIT
#                        Current limit in amps, default 1.2
#  -d DEBUG_LEVEL, --debug_level DEBUG_LEVEL
#                        Debug level, default 0
#  -m MIN_CURRENT, --min_current MIN_CURRENT
#                        Minimum current before charge end, default 0.06
#  -n, --no_float        No float state
#  -o OUTPUT, --output OUTPUT
#                        Log output pathname, default /b/batt/logs/spd3303s-
#                        charger.log
#  -p PER_CHARGE_LIMIT, --per_charge_limit PER_CHARGE_LIMIT
#                        per charge limit as fraction of capacity, default 0.3
#  -r RATED_CAPACITY, --rated_capacity RATED_CAPACITY
#                        Battery rated capacity, default 6.0
#  -t TIME_LIMIT, --time_limit TIME_LIMIT
#                        Charging time limit in seconds, default 10800
#  -u USB_PATH, --usb_path USB_PATH
#                        USB (instrument) device pathname, default /dev/usbtmc1

import argparse, os, sys, time, datetime

VERSION_STRING = '0.16'

FLOAT_EQUALITY_PERCENT = 1      # "close enough" % between two float values
RATED_CAPACITY = 1.0		# Battery amp hours
PER_CHARGE_LIMIT = 0.99         # Fraction of rated capacity allowed per charge

CHARGE_FRACTION = 0.3           # Fraction of capacity to use as charge current
CURRENT_LIMIT = RATED_CAPACITY * CHARGE_FRACTION # Charge current limit

MIN_CURRENT_FRACTION = 0.01     # Fraction of capacity to reach @ shutoff
MIN_CURRENT = RATED_CAPACITY * MIN_CURRENT_FRACTION 
MAX_CURRENT_RISE = 10           # Abort if current rises more than this %

PS_CHARGER_CHANNEL = 'CH1'	# Which PS channel used as charger
debug_level = 0			# Debug/verbosity level
CHARGE_VOLTAGE = 4.2	        # Target charge voltage
DEFAULT_CHARGING_TIME_LIMIT = 3600*3     # Charging time limit
PS_WRITE_DELAY = 0.25           # Seconds to pause after write to PS

charging_time_limit = DEFAULT_CHARGING_TIME_LIMIT      # Max time for a charge
DEFAULT_USB_PATH = '/dev/usbtmc1'		       # USBTMC interface for PS
DEFAULT_LOG_PATH = 'lithium-charger.log' # Log output path
out = None			# Logging output file
pwr = None			# Python Serial object for power supply
start_time = time.time()	# Float Unix Epoch seconds since Jan 1, 1970

class SPDException(Exception):
    "Exception base class"

class SPDWriteException(SPDException):
    "Failed to complete an SCPI command write to the power supply"
 
def float_equals(v1, v2):
    diff = abs(v1 - v2)
    if v1 > v2:
        temp = v1
    else:
        temp = v2
    return ((diff / temp) * 100.0) < FLOAT_EQUALITY_PERCENT

def myprint(s, console_only = True):
    "Print a string and flush output"
    print s
    sys.stdout.flush()
    if not console_only:
        try:
            os.write(out, '{0:s}\n'.format(s))
	except KeyboardInterrupt:
            finish('Keyboard Interrupt')
        except OSError:
            finish('tried to write to log but got OSError') 
        try:
            os.fsync(out)
	except KeyboardInterrupt:
            finish('Keyboard Interrupt')
        except OSError:
            finish('tried to fsync log but got OSError') 

def elapsed_time():
   "Return seconds since execution start"
   return int(time.time() - start_time)

def write_inst(s):
    "Write SCPI command to device. A write error is currently fatal."
    global pwr
    if debug_level > 0:
	myprint('write_inst: ' + s)
    try:
        os.write(pwr, s)
    except KeyboardInterrupt:
        raise
        finish('Keyboard Interrupt')
    except OSError:
        finish('tried to write ' + s + ' but got a write error')

def read_inst(cmd):
    "Write SCPI command to device, retrying as needed to compensate for Siglent"
    global pwr
    if debug_level > 0:
        myprint('read_inst: ' + cmd)
    try:
        s = os.read(pwr, 300)
    except KeyboardInterrupt:
        raise
        finish('Keyboard Interrupt')
    except OSError:
        myprint('read error: repeating query: ' + cmd)
        write_inst(cmd)
        time.sleep(PS_WRITE_DELAY)
        s = read_inst(cmd)
    if debug_level > 0:
        myprint('read_inst returning: ' + s)
    return s

def ask(cmd):
    "Write SCPI query and return response value"
    write_inst(cmd)
    time.sleep(PS_WRITE_DELAY)
    return read_inst(cmd)
 
def get_current():
    "Return PS output current"
    return float(ask('MEAS:CURR? ' + PS_CHARGER_CHANNEL))

def get_voltage():
    "Return PS output voltage"
    return float(ask('MEAS:VOLT? ' + PS_CHARGER_CHANNEL))

def get_current_limit():
    "Return PS current limit"
    return float(ask(PS_CHARGER_CHANNEL + ':CURR?'))

def get_voltage_limit():
    "Return PS voltage limit"
    return float(ask(PS_CHARGER_CHANNEL + ':VOLT?'))

def set_current_limit(current):
    "Change current limit"
    while True:
	try:
            write_inst(PS_CHARGER_CHANNEL + ':CURR '+ str(current))
            time.sleep(PS_WRITE_DELAY)
            if float_equals(get_current_limit(), current):
  	        return
        except KeyboardInterrupt:
            raise
            finish('Keyboard Interrupt')
	except:
	    supply_off()
            raise SPDWriteException

def set_voltage_limit(voltage):
    "Change voltage limit"
    while True:
	try:
            write_inst(PS_CHARGER_CHANNEL + ':VOLT ' + str(voltage))
            time.sleep(PS_WRITE_DELAY)
            if float_equals(get_voltage_limit(), voltage):
  	        return
        except KeyboardInterrupt:
            raise
            finish('Keyboard Interrupt')
	except:
	    supply_off()
            raise SPDWriteException

def supply_on():
    "Turn on the supply channel"
    write_inst('OUTP ' + PS_CHARGER_CHANNEL + ', ON')

def supply_off():
    "Turn off the supply channel"
    write_inst('OUTP ' + PS_CHARGER_CHANNEL + ', OFF')

def finish(msg):
    "Print optional message, turn off power supply channel, close paths, quit"
    if msg != '':
        print(msg)
    supply_off()
    os.close(pwr)
    os.close(out)
    quit()

def charge():
    "Charge."
    set_current_limit(current_limit)
    set_voltage_limit(CHARGE_VOLTAGE)
    supply_on()
    time.sleep(1.0)
    per_charge_coulombs = per_charge_limit * rated_capacity * 3600.0
    total_coulombs = 0.0
    current = get_current()
    minimum_current = current
    voltage = get_voltage()
    if debug_level > 0:
        myprint('Starting voltage ' + str(voltage))
        myprint('Starting current ' + str(current))
    if debug_level > 1:
        myprint('current: ' + str(current) + ' voltage: ' + str(voltage))
    while current > min_current:
	start_interval = time.time()
        relative_time = start_interval - start_time
        if relative_time >= charging_time_limit:
	    finish('charging time limit reached')
        now = datetime.datetime.now().isoformat(sep=' ')
	myprint(
	    str(now) + 
            " {0:6d} {1:5.2f} {2:6.3f}".format(relative_time,voltage, current))
	time.sleep(1.0)
        voltage = get_voltage()
	current = get_current()
        if debug_level > 1:
            myprint('current: ' + str(current) + ' voltage: ' + str(voltage))
	# Don't tolerate increase in current
        if current < minimum_current:
	    minimum_current = current
        elif current > minimum_current:
	    if (((current - minimum_current) / minimum_current) * 
	        100.0) > MAX_CURRENT_RISE:
	        finish('Charge aborted due to current rise')
	sys.stdout.flush()
	increment_time = time.time() - start_interval
	increment_coulombs = current * increment_time
	total_coulombs += increment_coulombs
	if total_coulombs >= per_charge_coulombs:
	    finish('Per-charge limit reached')

if __name__ == '__main__':
    "Handle command line arguments(options) and start charge"

    parser = argparse.ArgumentParser(
	description='Siglent SPD3303S-based Lithium Battery Charger ' + 'rev ' +
	VERSION_STRING)

    parser.add_argument(
	'-c', '--current_limit', default=str(CURRENT_LIMIT), 
	help='Current limit in amps, default ' + 
	str(CURRENT_LIMIT))

    parser.add_argument(
	'-d', '--debug_level', default='0', help='Debug level, default 0')

    parser.add_argument(
	'-m', '--min_current', default=str(MIN_CURRENT), 
	help='Minimum current before charge end, default ' + str(MIN_CURRENT))

    parser.add_argument(
	'-n', '--no_float', help='No float state', action='store_true')

    parser.add_argument(
	'-o', '--output', default=DEFAULT_LOG_PATH, help=
	'Log output pathname, default ' + str(DEFAULT_LOG_PATH))

    parser.add_argument(
	'-p', '--per_charge_limit', default=str(PER_CHARGE_LIMIT), 
	help='per charge limit as fraction of capacity, default ' + 
	str(PER_CHARGE_LIMIT))

    parser.add_argument(
	'-r', '--rated_capacity', default=RATED_CAPACITY, 
        help='Battery rated capacity, default ' + str(RATED_CAPACITY))

    parser.add_argument(
	'-t', '--time_limit', default=str(DEFAULT_CHARGING_TIME_LIMIT), 
	help='Charging time limit in seconds, default ' + 
	str(DEFAULT_CHARGING_TIME_LIMIT))

    parser.add_argument(
	'-u', '--usb_path', default=DEFAULT_USB_PATH, help=
	'USB (instrument) device pathname, default ' + str(DEFAULT_USB_PATH))

    args = parser.parse_args()

    debug_level = int(args.debug_level)
    charging_time_limit = int(args.time_limit)
    per_charge_limit = float(args.per_charge_limit)
    min_current = float(args.min_current)
    current_limit = float(args.current_limit)
    rated_capacity = float(args.rated_capacity)

    # If rated capacity overridden then compute reasonable charge current
    # and minimum current limits unless they are also overridden

    if not float_equals(rated_capacity, RATED_CAPACITY):
	if float_equals(current_limit, CURRENT_LIMIT):
	    current_limit = rated_capacity * CHARGE_FRACTION
	if float_equals(min_current, MIN_CURRENT):
	    min_current = rated_capacity * MIN_CURRENT_FRACTION

    myprint('open: ' + args.usb_path + ' ' + args.output)
    pwr = os.open(args.usb_path, os.O_RDWR)
    out = os.open(args.output, os.O_CREAT | os.O_WRONLY | os.O_APPEND, 0666)

    myprint('SPD-3303S Programmed Lithium Battery Charger')
    myprint('Using channel: ' + PS_CHARGER_CHANNEL)
    myprint('USB Path: ' + args.usb_path)
    myprint('LOG Path: ' + args.output)
    myprint('Rated battery capacity: ' + str(rated_capacity) + ' aH')
    myprint('Current limit: ' + str(current_limit) + ' A')
    myprint('Per Charge Amp Hours: ' + str(per_charge_limit * rated_capacity) + 
	    ' Ah')
    myprint('Charge cuttoff current: ' + str(min_current) + ' A')
    myprint('Charge voltage: ' + str(CHARGE_VOLTAGE) + ' V')

    charge()

    finish('')

